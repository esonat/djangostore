from abc import abstractmethod
from middleware.brand_service import get_store_brands
from products.models import Product,Category, Brand
from stores.models import Store
import logging
from middleware.category_service import get_store_category_trees,get_store_products_in_category,get_parent_categories,get_store_root_categories,get_store_subcategories
from middleware.brand_service import get_store_products_in_brands,get_store_brands_in_category
from middleware.utils import get_store_from_slug,get_category_from_slug

logger=logging.getLogger(__name__)



def get_store_items_in_category(store_slug,category_slug):
    store=get_store_from_slug(store_slug)
    category=get_category_from_slug(category_slug)

    products=[]

    #If category has children
    # if category.subcategories:
    #     for subcategory in category.subcategories.all():
    #         s=subcategory
    #         while(s.subcategories):
    #




class ProductsQueryFilter():

    def __init__(self,param_category=None,param_brand=None,store_slug=None):
        self.param_category=param_category
        self.param_brand=param_brand
        self.store_slug=store_slug
        self.result={}
        self.filter()

    def get_brands(self):
        brands_list=self.param_brand.split('-')
        brands=[]

        for brand_item in brands_list:
            brand=Brand.objects.get(slug=brand_item)
            brands.append(brand)

        return brands

    def filter(self):
        self.filter_init()

        if self.param_category:
            self.filter_category()
        if self.param_brand:
            self.filter_brand()

    def filter_init(self):
        store=Store.objects.get(slug=self.store_slug)

        products = store.products.all()
        root_categories = get_store_root_categories(self.store_slug)
        brands = get_store_brands(self.store_slug)

        self.result['products']=products
        self.result['categories']=root_categories
        self.result['brands']=brands
        self.result['store']=store

    def filter_category(self):
        self.result={}
        store=Store.objects.get(slug=self.store_slug)
        subcategories = get_store_subcategories(self.store_slug, self.param_category)
        category = Category.objects.get(slug=self.param_category)
        products = get_store_products_in_category(self.store_slug, self.param_category)
        parent_categories = get_parent_categories(self.param_category)
        brands=get_store_brands(self.store_slug)

        self.result['subcategories']=subcategories
        self.result['category']=category
        self.result['parent_categories']=parent_categories
        self.result['brands']=get_store_brands_in_category(self.store_slug,self.param_category)
        self.result['products']=products
        self.result['store'] = store

    def filter_brand(self):
        products_list= self.result['products']
        brands_list=self.param_brand.split('-')
        brands=self.get_brands()
        products = []

        for product in products_list:
            if product.brand in brands:
                products.append(product)

        self.result['products']=products


def get_most_sold_products():
    return Product.objects.filter(number_sold__gt=0).order_by('number_sold')


