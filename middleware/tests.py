from django.test import TestCase
from .toggles import Toggle
# Create your tests here.


class MiddlewareTestCase(TestCase):

    def test_set_toggle(self):
        Toggle.set_toggle('CART_ADD','enginsonat86@hotmail.com',True)
        self.assertEqual(Toggle.CART_ADD['enginsonat86@hotmail.com'],True)

    def test_get_toggle(self):
        self.assertEqual(Toggle.get_toggle('CART_ADD','deneme'),True)