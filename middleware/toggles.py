class Toggle:
    CART_ADD={}
    CART_EMPTY={}

    @staticmethod
    def set_toggle(code,user,value):
        if code=='CART_ADD':
            Toggle.CART_ADD[user]=value
        elif code=='CART_EMPTY':
            Toggle.CART_EMPTY[user]=value

    @staticmethod
    def get_toggle(code,user):
        if code=='CART_ADD':
            if user not in Toggle.CART_ADD:
                return True
            else:
                return Toggle.CART_ADD[user]
        elif code=='CART_EMPTY':
            if user not in Toggle.CART_EMPTY:
                return True
            else:
                return Toggle.CART_EMPTY[user]