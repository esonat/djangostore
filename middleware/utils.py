from stores.models import Store
from products.models import Category

def get_store_from_slug(slug):
    try:
        store=Store.objects.get(slug=slug)
        return store
    except Store.DoesNotExist:
        raise Exception('Store does not exist')


def get_category_from_slug(slug):
    try:
        category = Category.objects.get(slug=slug)
        return category
    except Category.DoesNotExist:
        raise Exception('Category does not exist')

