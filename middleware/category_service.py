from products.models import CategoryTree,CategoryNode
import logging
import sys
from middleware.utils import get_store_from_slug,get_category_from_slug


logging.basicConfig(stream=sys.stderr,level=logging.INFO)
logger=logging.getLogger(__name__)

def get_store_root_categories(slug):

    store=get_store_from_slug(slug)

    categories={}
    for product in store.products.all():
        category=product.category

        while(category.is_root==False):
            category=category.parent
        if category not in categories.keys():
            categories[category]=1
        elif category in categories.keys():
            val=categories[category]
            val+=1
            categories[category]=val

    return categories


def get_store_subcategories(store_slug,category_slug):
    store=get_store_from_slug(store_slug)
    category=get_category_from_slug(category_slug)

    subcategories={}
    for product in store.products.all():
        for child_category in category.subcategories.all():
            if product.category==child_category:
                if child_category not in subcategories.keys():
                    subcategories[child_category]=1
                else:
                    val=subcategories[child_category]
                    val+=1
                    subcategories[child_category]=val

    return subcategories



def form_category_tree(tree,category,parent,product_count):
    category_node=CategoryNode(category,parent,product_count)
    tree.add_node(category_node)

    if category.subcategories:
        for subcategory in category.subcategories.all():
            form_category_tree(tree,subcategory,category_node)


def get_store_category_trees(store_slug):
    store=get_store_from_slug(store_slug)
    root_categories=get_store_root_categories(store_slug)
    trees=[]

    for root,product_count in root_categories.items():
        tree=form_category_tree(CategoryTree(),root,None,product_count)
        trees.append(tree)

    return trees


def is_subcategory(subcategory,category):
    while(subcategory.parent):
        subcategory=subcategory.parent
        if subcategory==category:
            return True
    return False


def get_store_products_in_category(store_slug,category_slug):
    store=get_store_from_slug(store_slug)
    category=get_category_from_slug(category_slug)
    products=[]

    logger.error('Get Store Products')

    if category.subcategories.count()==0:
        logger.error('No subcategories')
        products.extend(store.products.all().filter(category=category))
    else:
        for product in store.products.all():
            c=product.category
            if(is_subcategory(c,category)):
                products.append(product)

    return products


def get_parent_categories(category_slug):
    category=get_category_from_slug(category_slug)
    categories=[]

    while(category.parent):
        category=category.parent
        categories.append(category)

    return categories