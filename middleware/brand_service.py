from products.models import Brand
from stores.models import Store
from middleware.utils import get_store_from_slug,get_category_from_slug
from middleware.category_service import is_subcategory

def get_store_brands(store_slug):
    store=Store.objects.get(slug=store_slug)
    brands=[]

    for product in store.products.all():
        if product.brand not in brands:
            brands.append(product.brand)

    return brands

def get_store_products_in_brands(store_slug,brand_list):
    store=Store.objects.get(slug=store_slug)
    brands=[]

    for brand_slug in brand_list:
        brand=Brand.objects.get(slug=brand_slug)
        brands.append(brand)

    products=[]

    for product in store.products.all():
        if product.brand in brands:
            products.append(product)

    return products


def get_store_brands_in_category(store_slug,category_slug):
    store=get_store_from_slug(store_slug)
    category=get_category_from_slug(category_slug)

    brands=[]

    for product in store.products.all():
        brand=product.brand
        if (product.category==category or is_subcategory(product.category,category))\
                and brand not in brands:
            brands.append(brand)

    return brands