from django.shortcuts import render
from django.views.generic import View
from core.utils import SendMailMixin
from contact.forms import ContactForm
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy,reverse

class ContactView(SendMailMixin,View):
    template_name='contact/contact_form.html'

    def get(self,request):
        form=ContactForm()
        return render(
            request,
            self.template_name,
            {'form':form})

    def post(self,request,*args,**kwargs):
        bound_form=ContactForm(request.POST)
        if bound_form.is_valid():
            keywords={
                'request':request,
                'subject':bound_form.cleaned_data['subject'],
                'message':bound_form.cleaned_data['message'],
                'from_email':bound_form.cleaned_data['sender'],
            }

            self.send_mail(**keywords)
        return redirect(reverse_lazy('contact_create'))