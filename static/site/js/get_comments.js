function get_comments(){
    $.ajax({
         type:"GET",
            url:"/ajax/get_comments?product_id="+$('.hdn_product_id').val(),
            dataType:"json",
             success: function (data) {
                $('.comment_panel').html(data.html);
            }
    });

         // CSRF code
    function getCookie(name) {
        var cookieValue = null;
        var i = 0;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (i; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        crossDomain:false,
        beforeSend: function(xhr,settings){
            if(!csrfSafeMethod(settings.type)){
                xhr.setRequestHeader("X-CSRFToken",csrftoken);
            }
        }
    });
}
