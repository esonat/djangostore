$(document).ready(function() {

    $('#remove_cart_item').click(function () {
        // var rating=$('.hdn_rating').val();
        // var name=$(".comment_name").val();
        // var text=$(".comment_text").val()
        // var title=$(".comment_title").val();
        //
        // if(!validateName(name) ||
        //     !validateTitle(title) ||
        //     !validateText(text)) return;
        var product_name=$(this).data("href");

        $.ajax({
            type:"POST",
            url:"/ajax/remove_cart_item/",
            dataType:"json",
            data:{
                 "product_name":product_name,
            },
             success: function (data) {
                 alert("Item deleted");
                 $("#table_body").empty().append(data);
                // $('.comment_panel').html(data.html);
            }
        });
    });
        // CSRF code
    function getCookie(name) {
        var cookieValue = null;
        var i = 0;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (i; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        crossDomain:false,
        beforeSend: function(xhr,settings){
            if(!csrfSafeMethod(settings.type)){
                xhr.setRequestHeader("X-CSRFToken",csrftoken);
            }
        }
    });
});