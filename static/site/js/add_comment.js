$(document).ready(function() {

    function validateName(name) {
        if(name.length<10){
            $(".comment_errors").html("<p>Name cannot be less than 10 characters");
            return false;
        }

        var regex = /^[A-Za-z ]+$/;
        if (name.match(regex)) {
            return true;
        } else {
            $(".comment_errors").html("<p>Name must have alphabet characters only");
            return false;
        }
    }

    function validateTitle(title) {

        if(title.length<5){
            $(".comment_errors").html("<p>Title cannot be less than 5 characters");
            return false;
        }

        var regex = /^[A-Za-z ]+$/;
        if (title.match(regex)) {
            return true;
        } else {
            $(".comment_errors").html("<p>Title must have alphabet characters only");
            return false;
        }
    }

    function validateText(text) {
        if (text.length < 5) {
            $(".comment_errors").html("<p>Text cannot be less than 5 characters");
            return false;
        }
        if (text.length > 50) {
            $(".comment_errors").html("<p>Text cannot be more than 50 characters");
            return false;
        }
        return true;
    }

    $('.btn_add_comment').click(function () {

        var rating=$('.hdn_rating').val();
        var name=$(".comment_name").val();
        var text=$(".comment_text").val()
        var title=$(".comment_title").val();

        if(!validateName(name) ||
            !validateTitle(title) ||
            !validateText(text)) return;

        $.ajax({
            type:"POST",
            url:"/ajax/add_comment/",
            dataType:"json",
            data:{
                 "name":name,
                 "text":text,
                 "title":title,
                 "product_id":$(".hdn_product_id").val(),
                 "rating":rating
            },
             success: function (data) {
                $('.comment_panel').html(data.html);
            }
        });
    });
        // CSRF code
    function getCookie(name) {
        var cookieValue = null;
        var i = 0;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (i; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        crossDomain:false,
        beforeSend: function(xhr,settings){
            if(!csrfSafeMethod(settings.type)){
                xhr.setRequestHeader("X-CSRFToken",csrftoken);
            }
        }
    });
});