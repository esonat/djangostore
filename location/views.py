from django.shortcuts import render
from django.shortcuts import render,redirect, render_to_response
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import View
from .models import Country
from .forms import CountryForm
from django.core.urlresolvers import reverse,reverse_lazy


class CountryList(ListView):
    template_name = 'products/country_list.html'
    paginate_by = 2

    def get(self,request):
        return render(request,
                      self.template_name,
                      {'country_list':Country.objects.all()})


class CountryCreate(CreateView):
    form_class = CountryForm
    model = Country
    template_name = 'products/country_create.html'


class CountryDetail(DetailView):
    form_class=CountryForm
    model=Country
    template_name='products/country_detail.html'


class CountryUpdate(UpdateView):
    template_name = 'products/country_update.html'
    form_class = CountryForm
    model=Country


class CountryDelete(DeleteView):
    model=Country
    success_url = reverse_lazy('country_list')