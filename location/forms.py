from django import forms
from .models import Country

class CountryForm(forms.ModelForm):
    class Meta:
        model=Country
        fields='__all__'

    def clean_slug(self):
        return self.cleaned_data['slug'].lower()