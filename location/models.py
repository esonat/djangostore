from django.core.validators import RegexValidator, MinValueValidator, MaxValueValidator,MinLengthValidator, MaxLengthValidator
from django.db import models
from django.urls import reverse
from django.conf import settings


class Country(models.Model):
    name=models.CharField(max_length=40,
                          validators=[
                                RegexValidator(regex=r'^[A-Za-z]*$',message='Country name must be only letters'),
                                MinLengthValidator(2,message='Country name cannot be less than 2 characters'),
                                MaxLengthValidator(30,message='Country name cannot be more than 30 characters'),
                                ],
                          unique=True)

    slug=models.SlugField(max_length=63,
                          help_text='A label for country',
                          unique=True)
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('country_detail',kwargs={'slug':self.slug})

    def get_update_url(self):
        return reverse('country_update',kwargs={'slug':self.slug})


class City(models.Model):
    name=models.CharField(max_length=30,validators=[
          MinLengthValidator(2,message="City name cannot be less than 2 characters"),
          MaxLengthValidator(30,message="City name cannot be more than 30 characters"),
          RegexValidator(regex=r'^[A-Za-z]*$', message='City name must be only letters'),
      ])
    country=models.ForeignKey(Country)
    slug=models.SlugField(max_length=63,help_text='Label for city',unique=True)

    def __str__(self):
        return self.name

class Address(models.Model):
    city=models.ForeignKey(City)
    street=models.CharField(max_length=30)
    state=models.CharField(max_length=30)
    zip=models.CharField(max_length=30)

    class Meta:
        unique_together=(("city","street"),)

    def __str__(self):
        return '{}-{}-{}'.format(self.city.name,self.state,self.street)