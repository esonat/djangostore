from django.contrib import admin

# Register your models here.
from location.models import Country, City, Address


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ('name','slug')
    list_filter=('name','slug')
    search_fields = ('name','slug')


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ('name','slug')
    list_filter=('name','slug')
    search_fields = ('name','slug')


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = ('city', 'street','state','zip')
    list_filter = ('city', 'street')
    search_fields = ('city', 'street','state')

