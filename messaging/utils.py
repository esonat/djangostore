import threading
#import pika
import logging
import json
from django.core import serializers
import redis
from messaging.models import MessageObject
from django.conf import settings
import time
from threading import Thread
from middleware.toggles import Toggle
logger=logging.getLogger(__name__)


class Listener(threading.Thread):
    def __init__(self,r,channels):
        threading.Thread.__init__(self)
        self.redis=r
        self.pubsub=self.redis.pubsub()
        self.pubsub.subscribe(channels)

    def work(self,item):
        if item['data']!=1:
            data=json.loads(item['data'].decode("utf-8"))
            code = data['code']
            message=data['message']
            user=data['user']
            logger.error("User: %s Type: %s Code: %s Message: %s Timestamp: %s" % (data['user'],data['type'],data['code'],data['message'],data['timestamp']))

            if code=='CART_ADD':
                if message=='true':
                    Toggle.set_toggle('CART_ADD',user,True)
                else:
                    Toggle.set_toggle('CART_ADD',user,False)
            elif code=='CART_EMPTY':
                if message=='true':
                    Toggle.set_toggle('CART_EMPTY',user,True)
                else:
                    Toggle.set_toggle('CART_EMPTY',user,False)

    def run(self):
        for item in self.pubsub.listen():
            if item['data']=="KILL":
                self.pubsub.unsubscribe()
                print(self,"unsubscribed")
                break
            else:
                self.work(item)
#
#
# class Listener:
#     @staticmethod
#     def listen(redis):
#         p=redis.pubsub()
#         p.subscribe('store-backend-result')
#
#         while True:
#             message=p.get_message()
#             if message:
#                 if not isinstance(message['data'],int):
#                     logger.error("REDIS MESSAGE:"+message['data'])

# def listen(redis):
#     p = redis.pubsub()
#     p.subscribe('store-backend-result')
#
#     while True:
#         message=p.get_message()
#         if message:
#             if not isinstance(message['data'],int):
#                 logger.error("REDIS MESSAGE:"+message['data'].decode("utf-8"))


class MessageManager(object):
    @staticmethod
    def send(user,type,code,message):
        r=redis.StrictRedis(
            host='localhost',
            port=settings.REDIS_PORT
        )
        #
        # listenerThread= Thread(target=listen,args=(r,))
        # listenerThread.start()
        #
        listener=Listener(r,['store-backend-result'])
        listener.start()

        dict={}

        dict['user']=user
        dict['type']=type
        dict['code']=code
        dict['message']=message
        dict['timestamp']=int(round(time.time() * 1000))

        r.publish('store-backend',dict)


# connection=pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
# channel=connection.channel()
# channel.queue_declare(queue='store-backend')
#
# #message_object=MessageObject('Event',message)
# #data = serializers.serialize("json", message_object)
#
# dict={}
# dict['type']='Event'
# dict['message']=message
#
# channel.basic_publish(exchange='',
#                       routing_key='routing_key',
#                       body='Engin')
# logger.error("MessageManager sent message to queue:"+queue)
# connection.close()