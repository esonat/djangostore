from django.contrib import admin
from order.models import Order

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('datetime','user','shipment','total_price')
    list_filter=('datetime','total_price')
    search_fields = ('user',)
    exclude = ('uuid',)