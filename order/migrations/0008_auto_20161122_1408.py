# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-22 14:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0007_auto_20161122_1407'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='uuid',
            field=models.CharField(default='1e648c06-e46e-4929-9ebd-8b7914cbe018', max_length=50, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='uuid',
            field=models.CharField(default='8235224a-eace-4f3d-babb-1d0db2555a8d', max_length=50, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='shipment',
            name='uuid',
            field=models.CharField(default='2d753b9b-5488-42d6-b40b-46b2957f6a50', max_length=50, primary_key=True, serialize=False),
        ),
    ]
