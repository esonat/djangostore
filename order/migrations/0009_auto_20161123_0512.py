# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-23 05:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0008_auto_20161122_1408'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='uuid',
            field=models.CharField(default='e1f2ac08-df3d-4ba4-879d-a0c161e5133b', max_length=50, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='uuid',
            field=models.CharField(default='517bf521-7bb1-446b-8d64-3ba7bcb1b415', max_length=50, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='shipment',
            name='uuid',
            field=models.CharField(default='a95156df-9a9d-4d64-9ea3-5fa72e1c0922', max_length=50, primary_key=True, serialize=False),
        ),
    ]
