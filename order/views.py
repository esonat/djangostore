from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.shortcuts import render,redirect, render_to_response
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import View
from django.core.urlresolvers import reverse,reverse_lazy
from django.contrib.auth import get_user, logout, get_user_model
from order.models import Order, OrderItem, Shipment
from products.forms import BrandForm, ProductForm
from products.models import Brand, Product
from django.core.urlresolvers import reverse,reverse_lazy
import uuid
import logging
from order.forms import OrderForm
from django.core import serializers
from django.core.mail import BadHeaderError,send_mail
from django.conf import settings


class OrderView(View):
    form_class=OrderForm
    template_name='order/order_form.html'

    def get(self,request):
        return render(request,
                      self.template_name,
                      {'form':self.form_class })

    def post(self,request,*args,**kwargs):
        bound_form = self.form_class(request.POST)

        if bound_form.is_valid():
            address=bound_form.save()

            order=Order()
            order.uuid=uuid.uuid4().__str__()
            order.user=get_user(request)

            shipment = Shipment()
            shipment.uuid = uuid.uuid4().__str__()
            shipment.status = 0
            shipment.address = address
            shipment.order = order
            order.shipment = shipment
            shipment.save()
            order.save()

            cart=serializers.deserialize('json',request.session['cart']).__next__().object

            for cart_item in cart.cartitem_set.all():
                order_item=OrderItem()
                order_item.uuid=uuid.uuid4().__str__()
                order_item.product=cart_item.product
                order_item.quantity=cart_item.quantity

                product = order_item.product
                quantity=order_item.quantity
                product.increment_number_sold(quantity)
                product.save()

                order_item.order=order
                order_item.price=cart_item.get_price()
                order_item.save()
                order.save()


            order.total_price=cart.get_total_price()
            order.save()

            del request.session['cart']
            send_mail('Order','Order placed\nID:'+shipment.uuid,settings.DEFAULT_FROM_EMAIL,[order.user.email],
                      fail_silently=False,)

        return redirect(reverse_lazy('product_list'))


class OrderDetailView(View):
    template_name='order/order_detail.html'

    def get(self,request,*args,**kwargs):
        order_id=kwargs['order_id']

        try:
            order=Order.objects.get(uuid=order_id)
        except Order.DoesNotExist:
            raise Http404
        
        #order=get_object_or_404(Order,uuid=order_id)
        order_items=order.orderitem_set.all()

        return render(request,self.template_name,
                      {'order':order,
                       'items':order_items})


class OrderHistoryView(View):
    template_name='order/order_history.html'

    def get(self,request,*args,**kwargs):
        user=get_user(request)
        orders=user.orders.all()

        return render(request,self.template_name,{'orders':orders})


class ShipmentDetail(View):
    template_name='order/shipment_detail.html'

    def get(self,request,*args,**kwargs):
        shipment_id = kwargs['shipment_id']
        shipment=Shipment.objects.get(uuid=shipment_id)

        return render(request,self.template_name,{'shipment':shipment})