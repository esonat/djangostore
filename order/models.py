from django.db import models
from django.conf import settings
from products.models import Product
from django.core.validators import RegexValidator, MinValueValidator, MaxValueValidator,MinLengthValidator, MaxLengthValidator
from location.models import Address
import uuid


class Shipment(models.Model):
    STATUS=(
        (0,'Not Delivered'),
        (1,'On Delivery'),
        (2,'Delivered'),
    )
    uuid=models.CharField(max_length=50,primary_key=True,default='6d264008-3db4-4e2f-87c4-562b7b1d9350')
    status=models.CharField(max_length=1,choices=STATUS)
    address=models.ForeignKey(Address)

    def get_status(self):
        return Shipment.STATUS.__getitem__(int(self.status))[1]
    def __str__(self):
        return self.address.street


class Order(models.Model):
    uuid=models.CharField(max_length=50,primary_key=True,default='32298b7f-b5e9-498e-bc78-14887b5cd59f')
    datetime=models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,related_name='orders',blank=True,default=None)
    shipment=models.OneToOneField(Shipment,on_delete=models.CASCADE)
    total_price = models.FloatField(default=1.0,
                              validators=[MinValueValidator(1.0, message="Order Total Price cannot be less than 1 dollars"),
                                          MaxValueValidator(100000.0,
                                                            message="Order Total Price cannot be more than 100000 dollars")])


class OrderItem(models.Model):
    uuid = models.CharField(max_length=50,primary_key=True,default='4e087b48-213a-4b20-9ce7-040794abccc4')
    product = models.ForeignKey(Product)
    quantity = models.IntegerField(validators=[
        MinValueValidator(1, message="Order Item quantity cannot be less than 1"),
        MaxValueValidator(1000, message="Order Item quantity cannot be more than 1000")])
    order = models.ForeignKey(Order)
    price = models.FloatField(default=1.0)
