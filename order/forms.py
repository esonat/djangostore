from django import forms

from location.models import Address


class OrderForm(forms.ModelForm):
    class Meta:
        model=Address
        fields='__all__'