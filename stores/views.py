from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render,redirect, render_to_response
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import View
from products.forms import BrandForm, ProductForm
from products.models import Brand, Product, Category
from django.core.urlresolvers import reverse,reverse_lazy
import uuid
import logging
from cart.models import Cart,CartItem
#logging.basicConfig()
from django.contrib.auth import get_user, logout, get_user_model
from django.core import serializers
from middleware.product_service import ProductsQueryFilter
# from middleware.category_service import get_store_category_trees,get_store_products_in_category,get_parent_categories
# Create your views here.
from stores.models import Store
from middleware.brand_service import get_store_brands


class StoreDetail(View):
    template_name='stores/store_detail.html'

    def get(self,request,*args,**kwargs):
        slug=kwargs['slug']
        param_order_by=request.GET.get('sort')
        param_category=request.GET.get('category')
        param_brand=request.GET.get('brands')

        filter=ProductsQueryFilter(param_category,param_brand,slug)

        return render(request,self.template_name,filter.result)

        # store=get_object_or_404(Store,slug=slug)
        # products=store.products.all()
        # root_categories=get_store_root_categories(slug)
        # brands=get_store_brands(slug)
        #
        #

        # if param_category is None:
        #     return render(request, self.template_name,
        #                   {'products': products, 'categories': root_categories,'brands':brands})
        # else:
        #     subcategories=get_store_subcategories(slug,param_category)
        #     category=Category.objects.get(slug=param_category)
        #     products=get_store_products_in_category(slug,param_category)
        #     parent_categories=get_parent_categories(param_category)
        #
        #     return render(request,self.template_name,
        #                   {'products':products,'parent_categories':parent_categories,
        #                    'category':category,'subcategories':subcategories,
        #                    'brands':brands})


class StoreList(View):
    template_name='stores/store_list.html'

    def get(self,request,*args,**kwargs):
        stores=Store.objects.all()
        page_number=request.GET.get('page')
        paginator=Paginator(stores,5)

        try:
            stores=paginator.page(page_number)
        except PageNotAnInteger:
            stores=paginator.page(1)
        except EmptyPage:
            stores=paginator.page(paginator.num_pages)

        return render(request,self.template_name,{'stores':stores})