from __future__ import unicode_literals
from django.core.validators import RegexValidator, MinValueValidator, MaxValueValidator,MinLengthValidator, MaxLengthValidator
from django.db import models
from location.models import Address
from django.urls import reverse
from django.conf import settings


class Store(models.Model):
    name=models.CharField(max_length=50)
    slug=models.SlugField(max_length=63,help_text='Text for store',unique=True)
    address=models.OneToOneField(Address,primary_key=True)
    points=models.FloatField(default=0.0,
                             validators=[MinValueValidator(0.0, message="Store points cannot be less than 1.0"),
                                         MaxValueValidator(10.0,message="Store points cannot be more than 10.0")])

    # def __init__(self):
    #     super().__init__()
    #     self.review_count=0

    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse('store_detail',kwargs={'slug':self.slug})
    def make_review(self,point):
        if self.review_count is None: self.review_count=0
        total_points=self.points*self.review_count

        self.review_count += 1
        points=(total_points+point)/self.review_count

    def get_points(self):
        points_str="%.1f" % self.points
        points_str=points_str.replace(".",",")
        return points_str