from django.contrib import admin
from django.contrib import admin
from .models import Store
import uuid
import logging

logger=logging.getLogger(__name__)


@admin.register(Store)
class StoreAdmin(admin.ModelAdmin):
    list_display = ('name','address','points')
    list_filter=('name','slug','points')
    search_fields = ('name','slug')