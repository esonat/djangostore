from django.test import TestCase
from .models import Store
from products.models import Product,Category
# Create your tests here.
from middleware.category_service import get_store_subcategories,get_store_products_in_category,get_parent_categories
from middleware.product_service import ProductsQueryFilter

class StoreTestCase(TestCase):
    fixtures = ['products/fixtures/initial_data.json','stores/fixtures/initial_data.json',]

    def test_get_categories(self):
        self.assertEqual(3,Product.objects.count())
        self.assertEqual(4,Category.objects.count())

    def test_get_store_subcategories(self):
        subcategories=get_store_subcategories('django-store','electronics')
        self.assertEqual(3,len(subcategories))
        subcategories=get_store_subcategories('django-store','television')
        self.assertEqual(0,len(subcategories))

    def test_get_store_products_in_category(self):
        products=get_store_products_in_category('django-store','electronics')
        self.assertEqual(3,len(products))
        products = get_store_products_in_category('django-store', 'television')
        self.assertEqual(1, len(products))

    def test_store_products(self):
        store = Store.objects.get(slug='django-store')
        self.assertEqual(3,store.products.count())

    def test_filter_store_products(self):
        store=Store.objects.get(slug='django-store')
        category=Category.objects.get(slug='television')

        products=store.products.all().filter(category=category)
        self.assertEqual(1,len(products))

    def test_get_store_points(self):
        store = Store.objects.get(slug='django-store')
        store.points=2.1654
        self.assertEqual('2,2',store.get_points())
    def test_get_parent_categories(self):
        parents=get_parent_categories('television')
        self.assertEqual(1,len(parents))
        parent=Category.objects.get(slug='electronics')
        self.assertEqual(parent,parents[0])

    def test_get_filter_results(self):
        category_param=None
        brand_param=None
        store=Store.objects.get(slug='django-store')

        filter=ProductsQueryFilter(category_param,brand_param,'django-store')
        self.assertEqual(4,len(filter.result))

