import subprocess
from django.test import TestCase
from django.test import Client
from django.core import serializers
from django.test.client import RequestFactory
from .utils import ModifySessionMixin
from products.models import Brand, Category, Product
from .models import Cart,CartItem
import uuid
# Create your tests here
import logging
from .utils import get_cart_items_count

logger=logging.getLogger(__name__)

class CartTestCase(ModifySessionMixin,TestCase):
    def setUp(self):

        self.create_session()
        self.factory=RequestFactory()
        brand = Brand.objects.create(
            name='TestBrand',
            slug='test_brand'
        )
        category = Category.objects.create(
            name='TestCategory',
            slug='test_category'
        )

        Product.objects.create(
            name='product',
            brand=brand,
            category=category,
            price=1.0,
            uuid=uuid.uuid4().__str__()
        )

    def test_add_to_cart(self):
        product=Product.objects.all()[0]
        product_id=product.uuid
        path='/products'
        url='/addtocart/'+product_id
        request=self.factory.post(url,{'quantity':'1','path':path})


    def test_get_empty_cart_item_count(self):
        client=Client()
        response=client.get('/ajax/get_cart_item_count/',None,HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual('0',response.content)

    def test_should_return_1_when_cart_contains_1_item(self):
        product = Product.objects.all()[0]
        product_id = product.uuid
        path = '/products'
        url = '/addtocart/' + product_id

        response= self.client.post(url, {'quantity': '1', 'path': path})
        #response = self.client.get('/ajax/get_cart_item_count/', None, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        session=self.client.session
        self.assertIsNotNone(session["cart"])
        # self.assertEqual('1', response.content)


        # client=Client()
        # response=client.post(url,{'quantity':'1','path':path})
        # self.assertEqual(301,response.status_code)

        #for item in client.session.items():


        #cart = serializers.deserialize('json', client.session["cart"]).__next__().object

        #self.assertEqual(1,cart.cartitem_set.count())
