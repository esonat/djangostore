from django.http import HttpResponse
import json
from django.core import serializers
from cart.models import Cart


def get_cart_item_count(request):
    if request.is_ajax():
        if 'cart' in request.session:
            cart = Cart()
            cart = serializers.deserialize('json', request.session['cart']).__next__().object
            data=cart.cartitem_set.count()
        else:
            data=0

        return HttpResponse(json.dumps(data), content_type='application/json')

def remove_cart_item(request):
    if request.is_ajax() and request.POST:
        if 'cart' in request.session:
            cart=Cart()
            cart = serializers.deserialize('json', request.session['cart']).__next__().object

            html=''
            product_name = request.POST.get('product_name')
            for item in cart.cartitem_set.all():
                if item.product.name==product_name:
                    item.delete()
                else:
                    html+='<tr><td>'+item.product.name+'</td>' \
                    '<td>'+str(item.quantity)+'</td>' \
                    '<td>'+str(item.get_price())+'</td>' \
                    '<td><button type="button" class="btn btn-danger" data-href="'+item.product.name+'" id="remove_cart_item">Remove</button></td></tr>'

            return HttpResponse(json.dumps(html),content_type='application/json')