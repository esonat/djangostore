from __future__ import unicode_literals
from django.core.validators import RegexValidator, MinValueValidator, MaxValueValidator,MinLengthValidator, MaxLengthValidator
from django.db import models
from products.models import Product
import uuid
from django.conf import settings
import json
from django.dispatch import receiver
from django.contrib.auth.signals import user_logged_out

import logging

logger=logging.getLogger(__name__)
    # def __init__(self):
    #     self.user=None
    #     self.items=[]
    #
    # def set_user(self,user):
    #     self.user=user
    #
    # def add_item(self,item):
    #     self.items.append(item)
    #
    # def get_items(self):
    #     return self.items
    #
    # def print_items(self):
    #     for item in self.items:
    #         print(item)
    #
    # def __repr__(self):
    #     return json.dumps(self.__dict__)
    #
    # def __str__(self):
    #     return self.user+'\'s Cart'
class Cart(models.Model):
    uuid = models.CharField(unique=True, max_length=50, primary_key=True, default='264cdad0-a917-497a-81c9-b49335f1fea2')
    user = models.ForeignKey(settings.AUTH_USER_MODEL,related_name='carts',blank=True,default=None)
    # total_price = models.FloatField(default=1.0,
    #                           validators=[MinValueValidator(1.0, message="Product price cannot be less than 1 dollars"),
    #                                       MaxValueValidator(50000.0,
    #                                                         message="Product price cannot be more than 50000 dollars")])

    def get_total_price(self):
        total_price=0
        for item in self.cartitem_set.all():
            total_price+=item.get_price()
        return total_price

class CartItem(models.Model):
    product=models.ForeignKey(Product)
    quantity=models.IntegerField(validators=[
        MinValueValidator(1,message="Cart Item quantity cannot be less than 1"),
        MaxValueValidator(1000,message="Cart Item quantity cannot be more than 1000")])
    cart=models.ForeignKey(Cart,blank=True,default=None,null=True)
    uuid = models.CharField(unique=True, max_length=50, primary_key=True, default='a3058728-a6ae-4334-b344-6c7ff8da6d49')

    def get_price(self):
        return self.product.price*self.quantity

#max_length=50
# def __init__(self,product,quantity):
    #     self.product=product
    #     self.quantity=quantity
    #
    # def set_quantity(self,quantity):
    #     self.quantity=quantity
    #
    # def __str__(self):
    #     return 'CartItem product:'+self.product.name
    #
    # def __repr__(self):
    #     return json.dumps(self.__dict__)



    # def __str__(self):
    #     return 'CartItem product:'+self.product.name


@receiver(user_logged_out)
def logout_remove_carts(sender,request,user,**kwargs):
    Cart.objects.all().delete()
    logger.error("LOGOUT SIGNAL")