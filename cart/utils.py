from importlib import import_module
from django.test import Client
from DjangoViews import settings
from django.core import serializers
from django.contrib.auth.signals import user_logged_out
import logging
from django.dispatch import receiver

from cart.models import Cart

logger=logging.getLogger(__name__)

class ModifySessionMixin(object):
    client=Client()

    def create_session(self):
        session_engine=import_module(settings.SESSION_ENGINE)
        store=session_engine.SessionStore()
        store.save()
        self.client.cookies[settings.SESSION_COOKIE_NAME]=store.session_key


def get_cart_items_count(request):
    cart = serializers.deserialize('json', request.session['cart']).__next__().object
    return cart.cartitem_set.count()

#
# user_logged_out.connect(logout_remove_carts)