import json
import base64
from channels.sessions import channel_session
from channels import Channel,Group
from channels.auth import http_session_user,channel_session_user,channel_session_user_from_http
from productuser.models import User
from cart.models import Cart,CartItem
#@channel_session_user_from_http

import logging

logger=logging.getLogger(__name__)

def connect_store(message,email):
    user=User.objects.get(email=base64.b64decode(email))
    try:
        cart=Cart.objects.filter(user=user).latest('uuid')
        item_count=cart.cartitem_set.count()
    except Cart.DoesNotExist:
        logger.error("CART DOES NOT EXIST")
        item_count=0

    Group(email).add(message.reply_channel)
    # message.reply_channel.send({
    #     "text":json.dumps("engin")
    #  })
    Group(email).send({
        "text":json.dumps(item_count)
    })

#@channel_session_user

def send_cart_items_count(message,email):
    # Group(message.user.username[0]).send({
    #     "text"
    # })
    message.reply_channel.send({
        "text":json.dumps(message.user.username[0])
    })

#@channel_session_user

def disconnect_store(message,email):
    Group(email).discard(message.reply_channel)