from django.contrib.messages import success
from django.shortcuts import render
from django.shortcuts import render,redirect, render_to_response
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import View
from middleware.toggles import Toggle
from products.forms import BrandForm, ProductForm
from products.models import Brand, Product
from location.forms import CountryForm
from location.models import Country
from django.core.urlresolvers import reverse,reverse_lazy
from django.contrib.auth import get_user, logout, get_user_model
from django.core import serializers
from cart.models import Cart,CartItem
import logging
import json
from django.forms.models import model_to_dict
import uuid
from middleware.product_service import get_most_sold_products
from messaging.utils import MessageManager

logger=logging.getLogger(__name__)

class AddToCartView(View):
    def post(self,request,*args,**kwargs):
        product_id=kwargs['product_id']
        item_quantity=int(request.POST['quantity'])
        path=request.POST['path']
        user=request.user.email

        try:
            MessageManager.send(request.user.email,"Event",'CART_ADD',"Item added to cart")
        except Exception:
            logger.error("MessageManager error while sending message")

        if Toggle.get_toggle('CART_ADD',user)==False:
            logger.error("CANNOT ADD ITEM TO CART")
            success(request,'Cannot add item to cart')
            return redirect(path)


        if 'cart' not in request.session:
            cart = Cart()
            cart.uuid=uuid.uuid4().__str__()
            cart_item=CartItem()
            cart_item.uuid=uuid.uuid4().__str__()

            cart.user=get_user(request)
            cart_item.product=Product.objects.get(uuid__iexact=product_id)
            cart_item.quantity=item_quantity
            cart.save()
            cart_item.cart=cart
            cart_item.save()


            #data=model_to_dict(cart)
            #request.session['cart']=json.dumps(data)
            request.session['cart']=serializers.serialize('json',[cart])
        else:
            cart=Cart()
            cart=serializers.deserialize('json',request.session['cart']).__next__().object

            for item in cart.cartitem_set.all():
                logger.error(item.__str__())

            product=Product.objects.get(uuid__iexact=product_id)
            product_exists=False

            for item in cart.cartitem_set.all():
                if item.product==product:
                    product_exists=True
                    logger.error("PRODUCT EXISTS")
                    quantity=item.quantity
                    item.quantity=quantity+item_quantity
                    item.cart=cart
                    item.save()
                    cart.save()


            if not product_exists:
                logger.error("DOES NOT EXIST")
                cart_item=CartItem()
                cart_item.uuid=uuid.uuid4().__str__()
                cart_item.product=product
                cart_item.quantity=item_quantity
                cart_item.cart=cart
                cart_item.save()
                cart.save()

            request.session['cart']=serializers.serialize('json',[cart])
            #request.session['cart']=cart

        success(request,'Item added to cart')

        return redirect(path)
            # return redirect(reverse('product_detail',
            #             kwargs={'product_id':product_id}))


class CartDetail(View):
    template_name='cart/cart_detail.html'

    def get(self,request,*args,**kwargs):
        products=get_most_sold_products()

        if 'cart' not in request.session:
            return render(self.request, self.template_name, {'most_sold_products':products,'cart_items': None})
        else:
            cart=Cart()
            #cart=json.loads(request.session['cart'])[0]
            # cart=Cart()
            cart = serializers.deserialize('json', request.session['cart']).__next__().object

            return render(self.request,self.template_name,{
                'most_sold_products': products,
                'cart':cart,
                'cart_items':cart.cartitem_set.all()})


''' Empty Cart'''
class CartEmptyView(View):
    def post(self,request,*args,**kwargs):
        user=request.user.email

        try:
            MessageManager.send(request.user.email, "Event", 'CART_EMPTY', "Cart emptied")
        except Exception:
            logger.error("MessageManager error while sending message")

        if Toggle.get_toggle('CART_EMPTY',user)==False:
            logger.error("CANNOT EMPTY CART")
            success(request, 'Cannot empty cart')
            return redirect(reverse_lazy('cart_detail'))

        if 'cart' in request.session:
            del request.session['cart']
            Cart.objects.all().delete()

        return redirect(reverse_lazy('cart_detail'))


class CartItemCountView(View):
    def get(self,request,*args,**kwargs):
        cart = serializers.deserialize('json', request.session['cart']).__next__().object
        return cart.cartitem_set.count()


class RemoveCartItemView(View):
    def post(self,request,*args,**kwargs):
        product_name=request.POST['product_name']
        path=request.POST['path']

        if 'cart' in request.session:
            cart=Cart()
            cart = serializers.deserialize('json', request.session['cart']).__next__().object

            for item in cart.cartitem_set.all():
                if item.product.name==product_name:
                    item.delete()

            if cart.cartitem_set.count()==0:
                Cart.objects.all().delete()


        success(request,"Item deleted")
        return redirect(path)