"""DjangoViews URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.views.generic import RedirectView

from DjangoViews.views import SlugView
from contact.views import ContactView
from order.views import OrderView, ShipmentDetail, OrderHistoryView, OrderDetailView
from products.views import BrandCreate, BrandDetail, BrandList, BrandUpdate, BrandDelete, ProductCreate, ProductDetail, \
    ProductUpdate, ProductDelete, ProductList, CategoryDetail
from location.views import CountryDetail, CountryList, CountryUpdate, CountryDelete, CountryCreate
from django.conf.urls import url,include
from productuser import urls as user_urls
from django.conf import settings
from django.conf.urls.static import static
from cart.views import AddToCartView,CartDetail,CartEmptyView, RemoveCartItemView
from core.views import page_not_found
from products.ajax import add_comment,get_comments
from django.conf.urls import (
    handler400, handler403, handler404, handler500
)

from stores.views import StoreDetail, StoreList
from cart.ajax import get_cart_item_count, remove_cart_item

handler404 = page_not_found

urlpatterns = [
    url(r'^$',RedirectView.as_view(
        pattern_name='product_list',
        permanent=False)),
    url(r'^stores/$',StoreList.as_view(),name='store_list'),
    url(r'^admin/', admin.site.urls),
    url(r'^user/',include(user_urls,
                          app_name='productuser',
                          namespace='dj-auth')),
    url(r'^category/(?P<slug>[\w\-]+)/$',CategoryDetail.as_view(),name='category_detail'),
    # url(r'^brands/$',BrandList.as_view(),name='brand_list'),
    #url(r'^brand/create/$',BrandCreate.as_view(),name='brand_create'),
    url(r'^brand/(?P<slug>[\w\-]+)/$',BrandDetail.as_view(),name='brand_detail'),
    #url(r'^brand/(?P<slug>[\w\-]+)/update/$',BrandUpdate.as_view(),name='brand_update'),
    #url(r'^brand/(?P<slug>[\w\-]+)/delete/$',BrandDelete.as_view(),name='brand_delete'),
    url(r'^products/$',ProductList.as_view(),name='product_list'),
    # url(r'^product/create/$',ProductCreate.as_view(),name='product_create'),
    url(r'^product/(?P<product_id>[^/]+)/$',ProductDetail.as_view(),name='product_detail'),
    # url(r'^product/(?P<product_id>[^/]+)/update/$',ProductUpdate.as_view(),name='product_update'),
    # url(r'^product/(?P<product_id>[^/]+)/delete/$', ProductDelete.as_view(), name='product_delete'),
    url(r'^countries/$',CountryList.as_view(),name='country_list'),
    # url(r'^country/create/$',CountryCreate.as_view(),name='country_create'),
    # url(r'^country/(?P<slug>[\w\-]+)/$',CountryDetail.as_view(),name='country_detail'),
    # url(r'^country/(?P<slug>[\w\-]+)/update/$',CountryUpdate.as_view(),name='country_update'),
    # url(r'^country/(?P<slug>[\w\-]+)/delete/$',CountryDelete.as_view(),name='country_delete'),
    url(r'^contact/$',ContactView.as_view(),name='contact_create'),
    url(r'^addtocart/(?P<product_id>[^/]+)/$',AddToCartView.as_view(),name='add_to_cart'),
    url(r'^cart/$',CartDetail.as_view(),name='cart_detail'),
    url(r'^cart/empty/$',CartEmptyView.as_view(),name='cart_empty'),
    url(r'^order/$',OrderView.as_view(),name='order_form'),
    url(r'^shipment/(?P<shipment_id>[^/]+)/$',ShipmentDetail.as_view(),name='shipment_detail'),
    url(r'^order/history/$',OrderHistoryView.as_view(),name='order_history'),
    url(r'^order/(?P<order_id>[^/]+)/$',OrderDetailView.as_view(),name='order_detail'),
    url(r'^(?P<slug>[\w\-]+)/$',SlugView.as_view(),name='slug_view'),
    url(r'^ajax/add_comment/$',add_comment),
    url(r'^ajax/get_comments/$',get_comments),
    url(r'^ajax/get_cart_item_count/$',get_cart_item_count),
    url(r'^ajax/remove_cart_item/$',remove_cart_item),
    url(r'^store/(?P<slug>[\w\-]+)/$',StoreDetail.as_view(),name='store_detail'),
    url(r'^cart/remove_cart_item/$',RemoveCartItemView.as_view(),name='remove_cart_item'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
              #+ static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)


#
# urlpatterns += patterns('',
#         (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
#         'document_root': settings.MEDIA_ROOT}))
