from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.urls import reverse_lazy
from django.shortcuts import render,redirect, render_to_response
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import View
from django.core.urlresolvers import reverse,reverse_lazy
from products.models import Brand, Category


def handler404(request):
    return redirect(reverse_lazy('product_list'))


class SlugView(View):
    def get(self,request,*args,**kwargs):
        slug=kwargs['slug']

        try:
            brand=Brand.objects.get(slug=slug)
            return redirect(reverse('brand_detail',kwargs={'slug':slug}))
        except Brand.DoesNotExist:
            pass

        try:
            category=Category.objects.get(slug=slug)
            return redirect(reverse('category_detail',kwargs={'slug':slug}))
        except Category.DoesNotExist:
            pass

        return render_to_response('404.html')

