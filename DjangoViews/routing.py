from channels import route
from cart.consumers import connect_store,disconnect_store,send_cart_items_count

#r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"

channel_routing=[
    route("websocket.connect",connect_store,
          path=r'^/stream/(?P<email>.+)/$'),
    route("websocket.disconnect",disconnect_store,
          path=r'^/stream/(?P<email>.+)/$'),
    route("websocket.receive",send_cart_items_count,
          path=r'^/stream/(?P<email>.+)/$'),
]