from django.test import TestCase
from products.models import Product, Brand, Category
import uuid


class ProductTestCase(TestCase):
    def setUp(self):
        brand=Brand.objects.create(
            name='TestBrand',
            slug='test_brand'
        )
        category=Category.objects.create(
            name='TestCategory',
            slug='test_category'
        )

        Product.objects.create(
            name='product',
            brand=brand,
            category=category,
            price=1.0,
            uuid=uuid.uuid4().__str__()
        )

    def test_random_product(self):
        product=Product.objects.all()[0]
        self.assertIsNotNone(product)
        self.assertEqual('product',product.name)
        self.assertEqual('TestBrand',product.brand.name)
        self.assertEqual('TestCategory', product.category.name)
        self.assertEqual(1.0,product.price)