import json

from django.http import Http404
from django.http import HttpResponse
from .models import Comment, Product
import logging
from products.helpers import show_star

logger=logging.getLogger(__name__)

def add_comment(request):
    if request.is_ajax() and request.POST:
        title=request.POST.get('title')
        text=request.POST.get('text')
        product_id=request.POST.get('product_id')
        rating_str=request.POST.get('rating')
        rating=float(rating_str)

        for key in request.POST:
            value = request.POST[key]
            logger.error(key+":"+value)

        # logger.error('title:'+title)
        #logger.error(rating)
        #
        product=Product.objects.get(uuid=product_id)

        comment=Comment()
        comment.product=product
        comment.title=title
        comment.text=text
        comment.rating=rating

        comment.save()

        comments=Comment.objects.filter(product=product)
        html=''

        for item in comments:
            html+='<b>'+item.title+'</b><p>'+show_star(item.rating,15)+'</p><p>'+item.text+'</p>'

        data={'html':html}
        #data={'message':'%s added' % request.POST.get('text')}
        return HttpResponse(json.dumps(data),content_type='application/json')
    else:
        raise Http404


def get_comments(request):
    if request.is_ajax and request.GET:
        product_id=request.GET.get('product_id')

        product=Product.objects.get(uuid=product_id)
        comments = Comment.objects.filter(product=product)
        html = ''

        image_path=product.get_image_path()
        # html+=  '<img id="img_comment_summary" src="' + image_path + '" width="300" height="300"/>'
        # '<div id="comment_5">Perfect</div>'
        # '<div id="comment_4">Very Good</div>'
        # '<div id="comment_3">Good</div>'
        # '<div id="comment_2">Not Bad</div>'
        # '<div id="comment_1">Very Bad</div>'

        for item in comments:
            html += '<b>' + item.title + '</b><p>' + show_star(item.rating,15) + '</p><p>' + item.text + '</p>'

        data = {'html': html}
        # data={'message':'%s added' % request.POST.get('text')}
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        raise Http404
