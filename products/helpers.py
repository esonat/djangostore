import math

def show_star(value,size):
    empty = 'http://localhost:8000/media/images/empty-star.png'
    half = 'http://localhost:8000/media/images/half-star.png'
    full = 'http://localhost:8000/media/images/full-star.png'
    size=str(size)

    if float(value)==0.0:
        href='http://localhost:8000/media/images/empty-star.png'
        html=''

        for i in range(5):
            html+="<img src='"+empty+"' width='"+size+"' height='"+size+"'/>"

        return html

    if float(value)==int(value):
        v=int(value)
        html=''
        for i in range(v):
            html+="<img src='"+full+"' width='"+size+"' height='"+size+"'/>"

        if v<5:
            for i in range(v,5):
                html+="<img src='"+empty+"' width='"+size+"' height='"+size+"'/>"

        return html

    v=float(value)
    if(math.ceil(v)-v <= v-math.floor(v)):
        html=''
        floor=math.floor(v)

        for i in range(math.floor(v)):
            html+="<img src='"+full+"' width='"+size+"' height='"+size+"'/>"
        html+="<img src='"+half+"' width='"+size+"' height='"+size+"'/>"

        if floor+1<5:
            for i in range(floor+1, 5):
                html+="<img src='"+empty+"'/>"

        return html
    else:
        html = ''
        for i in range(math.floor(v)):
            html+="<img src='"+full+"' width='"+size+"' height='"+size+"'/>"

        if math.floor(v)<5:
            for i in range(math.floor(v),5):
                html+="<img src='"+empty+"' width='"+size+"' height='"+size+"'/>"

        return html
