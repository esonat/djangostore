from django import forms

from products.models import Brand, Product
from django.forms import ModelForm,Textarea
from django.utils.translation import ugettext_lazy as _
import logging


logger=logging.getLogger(__name__)

class BrandForm(forms.ModelForm):
    class Meta:
        model=Brand
        fields='__all__'
        labels={
            'name':_('Name'),
        }
        help_texts={
            'name': _('Brand name'),
        }
        error_messages={
            'name':{
                'max_length': _("Brand name is too long"),
            },
        }
        #
        # widgets={
        #     'name':Textarea(attrs={'cols':20,'rows':20}),
        # }

    def clean_slug(self):
        return self.cleaned_data['slug'].lower()


class ProductForm(forms.ModelForm):
    # image=forms.ImageField(label='Product image',
    #                        help_text='max. 50 MB')

    class Meta:
        model=Product
        exclude=('uuid','rating',)

        def __init__(self,*args,**kwargs):
            super(ProductForm,self).__init__(*args,**kwargs)
            self.fields['brand']=forms.ModelChoiceField(
                empty_label= None,
                queryset=Brand.objects.all())

