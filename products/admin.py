from django.contrib import admin
from .models import Product,Brand, Category
import uuid
import logging

logger=logging.getLogger(__name__)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name','brand','price','image')
    list_filter=('name','price')
    search_fields = ('name','brand')
    exclude = ('uuid','rating',)
    #
    # def save_model(self, request, obj, form, change):
    #     obj.uuid=uuid.uuid4().__str__()
    #     obj.save()

    # def save_form(self, request, form, change):
    #     product=form.save(commit=False)
    #     product.uuid=uuid.uuid4().__str__()
    #
    #     logger.error(product.uuid)
    #     product.save()
    def save_formset(self, request, form, formset, change):
        product=formset.save(commit=False)
        product.uuid=uuid.uuid4().__str__()
        product.save()


@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin):
    list_display = ('name','slug')
    list_filter=('name','slug')
    search_fields = ('name','slug')
    empty_value_display='empty'


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name','slug')
    list_filter=('name','slug')
    search_fields = ('name','slug')
    empty_value_display='empty'






