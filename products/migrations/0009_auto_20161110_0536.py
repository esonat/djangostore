# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-10 05:36
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0008_auto_20161108_1442'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='rating',
            field=models.FloatField(default=0, validators=[django.core.validators.MinValueValidator(0, message='Product rating cannot be less than 0'), django.core.validators.MaxValueValidator(5, message='Product rating cannot be more than 5')]),
        ),
        migrations.AlterField(
            model_name='product',
            name='uuid',
            field=models.CharField(default='18925d28-efdc-4205-8542-c22203c2f5aa', max_length=50, primary_key=True, serialize=False),
        ),
    ]
