from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render,redirect, render_to_response
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import View
from products.forms import BrandForm, ProductForm
from products.models import Brand, Product, Category
from django.core.urlresolvers import reverse,reverse_lazy
import uuid
import logging
from cart.models import Cart,CartItem
#logging.basicConfig()
from django.contrib.auth import get_user, logout, get_user_model
from django.core import serializers

logger=logging.getLogger(__name__)

class BrandCreate(CreateView):
    form_class=BrandForm
    model=Brand
    template_name = 'products/brand_create.html'
    success_url = reverse_lazy('brand_list')

class BrandDetail(DetailView):
    form_class = BrandForm
    model = Brand


class BrandList(ListView):
    template_name = 'products/brand_list.html'
    paginate_by = 2

    def get(self,request):
        return render(request,self.template_name,
                      {'brand_list':Brand.objects.all()})


class BrandUpdate(UpdateView):
    template_name_suffix = '_update'
    form_class = BrandForm
    model=Brand


class BrandDelete(DeleteView):
    model=Brand
    success_url = reverse_lazy('brand_list')


# Products

class ProductCreate(CreateView):
    form_class=ProductForm
    model=Product
    template_name = 'products/product_create.html'

    def post(self,request,*args,**kwargs):
        form=ProductForm(request.POST,request.FILES)
        if form.is_valid():
            name=form.cleaned_data.get('name')
            brand=form.cleaned_data.get('brand')

            product=form.save(commit=False)
            product.name=name
            product.brand=brand
            product.uuid = uuid.uuid4().__str__()
            product.save()

            return redirect(product.get_absolute_url())
        else:
            # for error in form.non_field_errors():
            #     logger.error(error)
            # for error in form.errors:
            #     logger.error()

            return render(
                request,
                self.template_name,
                {'form':form})



class ProductDetail(DetailView):
    form_class=ProductForm
    model=Product
    template_name = 'products/product_detail.html'

    def get(self,*args,**kwargs):
        self.product_id=kwargs['product_id']
        super(ProductDetail,self).get(*args,**kwargs)

        return render(self.request,self.template_name,{'product':self.get_object()})

    def get_object(self):
        #product_id=self.request.GET.get('product_id')
        object=Product.objects.filter(uuid__iexact=self.product_id)[0]
        return object



class ProductUpdate(UpdateView):
    template_name='products/product_update.html'
    form_class = ProductForm
    model=Product
    pk_url_kwarg='product_id'


# def get(self,*args,**kwargs):
#     self.product_id=kwargs['product_id']
#     super(ProductUpdate,self).get(*args,**kwargs)
#     product=self.get_object()
#     form=self.form_class(initial={'name':product.name,
#                                   'brand':product.brand})
#
#     return render(self.request,
#                   self.template_name,
#                   {'form':form})
#     #
#     # return render(self.request,
#     #               self.template_name,
#     #               {'form':self.form_class(product)})
#
# def get_object(self):
#     logger.error(self.product_id)
#     object=Product.objects.get(uuid__iexact=self.product_id)
#     return object


class ProductDelete(DeleteView):
    model = Product
    success_url = reverse_lazy('product_list')
    pk_url_kwarg = 'product_id'


class ProductList(ListView):
    template_name = 'products/product_list.html'


    def get(self,request):
        products=Product.objects.all()
        page_number=request.GET.get('page',1)
        paginator=Paginator(products,10)

        try:
            products=paginator.page(page_number)
        except PageNotAnInteger:
            products=paginator.page(1)
        except EmptyPage:
            products=paginator.page(paginator.num_pages)

        return render(request, self.template_name,
                      {'product_list': products})


class CategoryDetail(View):
    template_name='products/category_detail.html'

    def get(self,request,*args,**kwargs):
        slug=kwargs['slug']
        try:
            category=Category.objects.get(slug=slug)
        except Category.DoesNotExist:
            return render_to_response('404.html')

        products=category.product_set.all()
        return render(request,self.template_name,{'products':products})
