from django import template
import math

register=template.Library()


def show_star(value):
    empty = 'http://localhost:8000/media/images/empty-star.png'
    half = 'http://localhost:8000/media/images/half-star.png'
    full = 'http://localhost:8000/media/images/full-star.png'

    if float(value)==0.0:
        href='http://localhost:8000/media/images/empty-star.png'
        html=''

        for i in range(5):
            html+="<img src='"+empty+"'/>"

        return html

    if float(value)==int(value):
        v=int(value)
        html=''
        for i in range(v):
            html+="<img src='"+full+"'/>"

        if v<5:
            for i in range(v,5):
                html+="<img src='"+empty+"'/>"

        return html

    v=float(value)
    if(math.ceil(v)-v <= v-math.floor(v)):
        html=''
        floor=math.floor(v)

        for i in range(math.floor(v)):
            html+="<img src='"+full+"'/>"
        html+="<img src='"+half+"'/>"

        if floor+1<5:
            for i in range(floor+1, 5):
                html+="<img src='"+empty+"'/>"

        return html
    else:
        html = ''
        for i in range(math.floor(v)):
            html+="<img src='"+full+"'/>"

        if math.floor(v)<5:
            for i in range(math.floor(v),5):
                html+="<img src='"+empty+"'/>"

        return html

register.filter('show_star',show_star)