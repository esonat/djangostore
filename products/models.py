from __future__ import unicode_literals
from django.core.validators import RegexValidator, MinValueValidator, MaxValueValidator,MinLengthValidator, MaxLengthValidator
from django.db import models
from django.urls import reverse
from django.conf import settings
import uuid
import json
from jsonfield import JSONField
import logging

from stores.models import Store

logger=logging.getLogger(__name__)


class Brand(models.Model):
    name=models.CharField(max_length=30)
    slug=models.SlugField(max_length=63,
                          help_text='A label for brand',
                          unique=True)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse('brand_detail',kwargs={'slug':self.slug})
    def get_update_url(self):
        return reverse('brand_update',kwargs={'slug':self.slug})


class Category(models.Model):
    name=models.CharField(max_length=30)
    slug=models.SlugField(max_length=63,help_text='Category label',unique=True)
    parent=models.ForeignKey("Category",null=True,blank=True,default=None,related_name='subcategories')

    @property
    def is_root(self):
        return self.parent is None

    def __str__(self):
        return self.name


class CategoryNode():
    def __init__(self,category,parent,product_count):
        self.category=category
        self.parent=parent
        self.children=[]
        self.product_count=product_count

    @property
    def get_type(self):
        if self.category.subcategories:
            if self.parent is None:
                return 'root'
            else:
                return 'intermediate'
        else:
            return 'product'

    def add_node(self,node):
        node.parent=self
        self.children.append(node)


class CategoryTree():
    def __init__(self,root=None):
        self.root=root

    def add_node(self,node):
        if self.root is None:
            node.parent=None
            self.root=node
        else:
            node.parent=self
            self.root.add_node(node)

#'4850f23f-217a-4546-a557-f7802c833263'
class Product(models.Model):
    name=models.CharField(max_length=30)
    brand=models.ForeignKey(Brand)
    uuid=models.CharField(max_length=50)
    image=models.ImageField(upload_to='images/',blank=True,default=None)
    price=models.FloatField(default=1.0,
        validators=[MinValueValidator(1.0,message="Product price cannot be less than 1 dollars"),
                    MaxValueValidator(50000.0,message="Product price cannot be more than 50000 dollars")])
    category=models.ForeignKey(Category,related_name="products")
    rating=models.FloatField(default=0,validators=[
        MinValueValidator(0,message="Product rating cannot be less than 0"),
        MaxValueValidator(5,message="Product rating cannot be more than 5")])
    warranty=models.IntegerField(default=None,blank=True,null=True)
    details=JSONField(default=None)
    store=models.ForeignKey(Store,related_name='products',default=1)
    number_sold=models.IntegerField(default=0,validators=[
        MinValueValidator(0, message="Product number sold cannot be less than 0"),
        MaxValueValidator(50000, message="Product number sold cannot be more than 50000")])

    def get_absolute_url(self):
        return reverse('product_detail',kwargs={'product_id':self.uuid})

    def get_update_url(self):
        return reverse('product_update',kwargs={'product_id':self.uuid})

    def get_image_path(self):
        if self.image:
            return 'http://localhost:8000'+settings.MEDIA_URL+self.image.__str__()
    def get_details_dictionary(self):
        result={}
        result['RAM']='4GB'
        return result

    def set_rating(self):
        rating_avg = 0
        for comment in self.comment_set.all():
            rating_avg += comment.rating
        rating_avg = rating_avg / self.comment_set.count()
        self.rating=rating_avg
        self.save()
    def increment_number_sold(self,value):
        self.number_sold+=value

class Comment(models.Model):
    title=models.CharField(default=None,max_length=30)
    text=models.TextField(default=None,max_length=100)
    datetime=models.DateTimeField(auto_now_add=True)
    rating=models.IntegerField(default=0,validators=[
        MaxValueValidator(5,message="Comment rating cannot be more than 5"),
        MinValueValidator(0,message="Comment rating cannot be negative")
    ])
    product=models.ForeignKey(Product)


    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(Comment, self).save(force_insert, force_update)
        self.product.set_rating()
        logger.error("Comment Save called")
        logger.error(self.product.rating)


    def delete(self, using=None, keep_parents=False):
        self.product.set_rating()
        logger.error("Comment delete called")
        logger.error(self.product.rating)
        super(Comment,self).delete(keep_parents)
