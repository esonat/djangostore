from django.shortcuts import render, render_to_response, redirect

# Create your views here.
from django.template import RequestContext
from django.urls import reverse_lazy


def page_not_found(request):
    response=render_to_response('404.html',context=RequestContext(request))
    response.status_code=404

    return redirect(reverse_lazy('product_list'))