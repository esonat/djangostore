from django.contrib.auth import get_user
from django.core.exceptions import ValidationError
from django.template.loader import render_to_string
from django.contrib.auth.tokens import default_token_generator as token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.conf import settings
from django.core.mail import BadHeaderError,send_mail
from logging import CRITICAL,ERROR
from smtplib import SMTPException
import logging
import traceback

logger=logging.getLogger(__name__)

class SendMailMixin:

    def log_mail_error(self,**kwargs):
        msg='Email did not send'
        'from_email: '+kwargs.get('from_email'),
        'subject: '+kwargs.get('subject'),
        'message: '+kwargs.get('message')

        if 'error' in kwargs:
            level=ERROR
            error_msg=(
                'error {0.__class__.__name__}\n'
                'args: {0.args}\n')
            error_info=error_msg.format(kwargs['error'])
            msg+=error_info
        else:
            level=CRITICAL

        logger.log(level,msg)


    @property
    def mail_sent(self):
        if hasattr(self,'_mail_sent'):
            return self._mail_sent
        return False

    @mail_sent.setter
    def set_mail_sent(self,value):
        raise TypeError('Cannot set mail_sent attribute')

    def _send_mail(self, request, **kwargs):
        #kwargs['context'] = self.get_context_data(request, user)
        mail_kwargs = {
            "subject": kwargs.get('subject'),
            "message": kwargs.get('message'),
            "from_email": kwargs.get('from_email'),
            "recipient_list": [settings.DEFAULT_TO_EMAIL],
        }
        try:
            number_sent = send_mail(**mail_kwargs)
        except Exception as error:
            self.log_mail_error(error=error, **mail_kwargs)
            if isinstance(error, BadHeaderError):
                err_code = 'badheader'
            elif isinstance(error, SMTPException):
                err_code = 'smtperror'
            else:
                err_code = 'unexpectederror'
            return (False, err_code)
        else:
            if number_sent > 0:
                return (True, None)

        self.log_mail_error(**mail_kwargs)
        return (False, 'unknownerror')

    def send_mail(self,**kwargs):
        request=kwargs.pop('request',None)

        # for key in kwargs:
        #     logger.error(key,kwargs[key])

        if request is None:
            tb=traceback.format_stack()
            tb=[' '+line for line in tb]
            logger.warning('send_mail called '
                           ' without request.\n'
                           'Traceback\n{}'.format(''.join(tb)))
            self._mail_sent=False
            return self.mail_sent

        self._mail_sent,error=(
            self._send_mail(request,**kwargs))

        if not self.mail_sent:
            self.add_error(
                None,
                ValidationError(self.mail_validation_error,
                                code=error))
        return self.mail_sent

